using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV2
{
    class Die
    {
        //1.ZADATAK
        //private int numberOfSides;
        //private Random randomGenerator;
        //public Die(int numberOfSides)
        //{
        //    this.numberOfSides = numberOfSides;
        //    this.randomGenerator = new Random();
        //}
        //public int Roll()
        //{
        //    int rolledNumber;
        //    rolledNumber = randomGenerator.Next(1, numberOfSides + 1);
        //    return rolledNumber;
        //}

        //2.ZADATAK
        //private int numberOfSides;
        //private Random randomGenerator;
        //public Die(int numberOfSides, Random randomGenerator)
        //{
        //    this.numberOfSides = numberOfSides;
        //    this.randomGenerator = randomGenerator;
        //}
        //public int Roll()
        //{
        //    int rolledNumber;
        //    rolledNumber = randomGenerator.Next(1, numberOfSides + 1);
        //    return rolledNumber;
        //}


        //3.ZADATAK
        private int numberOfSides;
        private RandomGenerator randomGenerator;
        public Die(int numberOfSides, RandomGenerator randomGenerator)
        {
            this.numberOfSides = numberOfSides;
            this.randomGenerator = randomGenerator;
        }
        public int Roll()
        {
            int rolledNumber;
            rolledNumber = randomGenerator.NextInt(1, numberOfSides + 1);
            return rolledNumber;
        }
    }
}
