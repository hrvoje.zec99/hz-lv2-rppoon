using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV2
{
    class FileLogger:ILogger
    {
        private string filePath;
            public FileLogger(string filePath) {
             this.filePath = filePath;
            }
        public void Log(string message) {
            using (System.IO.StreamWriter fileWriter = new System.IO.StreamWriter(this.filePath, true)) {
                fileWriter.WriteLine(message);
            }
        }
    }
}
