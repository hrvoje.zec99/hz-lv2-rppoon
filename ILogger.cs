namespace RPPOON_LV2
{
    internal interface ILogger
    {
        void Log(string message);
    }
}