using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV2
{
    class Program
    {
        static void Main(string[] args)
        {
            //3.ZADATAK
            RandomGenerator randomGen = RandomGenerator.GetInstance();
            //2.ZADATAK
            Random randomGenerator = new Random();
            //1.ZADATAK

            DiceRoller diceRoller = new DiceRoller();
           
            for(int i = 0; i < 20; i++)
            {
                diceRoller.InsertDie(new Die(6,/*,2.ZADATAK randomGenerator*//*(3.zadatak)*/ randomGen));
            }
            
            diceRoller.RollAllDice();
             
            IList<int> results = diceRoller.GetRollingResults();
            foreach(int result in results)
            {
                Console.WriteLine(result);
            }
        }
    }
}
